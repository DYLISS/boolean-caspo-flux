# boolean-caspo-flux


### Installation
```
docker pull registry.gitlab.inria.fr/dyliss/boolean-caspo-flux/boolean-caspo-flux:cmsb2021
```

### Manual
To get more details, see [https://github.com/bioasp/boolean-caspo-flux](https://github.com/bioasp/boolean-caspo-flux).
